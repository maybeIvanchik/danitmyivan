"use strict";

function calculator(a, b, op) {
  let res;
  do {
    a = prompt("Print first number", a);
    b = prompt("Print second number", b);
    op = prompt("Print operation");
  } while (
    isNaN(a) ||
    isNaN(b) ||
    a === "" ||
    b === "" ||
    a === null ||
    b === null
  );

  if (op === "+") {
    res = +a + +b;
  } else if (op === "-") {
    res = a - b;
  } else if (op === "/") {
    res = a / b;
  } else if (op === "*") {
    res = a * b;
  }

  console.log(`Result: ${res}.`);
}

calculator();

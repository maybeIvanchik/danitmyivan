"use strict";

let userNum, myNum;

do {
  userNum = prompt("Print your number");
} while (userNum % 1 !== 0);

if (userNum >= 5) {
  for (myNum = 0; myNum <= userNum; myNum++) {
    if (myNum % 5 === 0) {
      console.log(myNum);
    }
  }
} else if (userNum % 5 !== 0) {
  alert("Sorry, no numbers");
}
